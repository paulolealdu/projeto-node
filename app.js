const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Rotas para clientes
app.get('/clientes', (req, res) => {
    const source = req.query;

    return res.send(`Cliente encontrado: ${source.nome}.`);
});

app.post('/clientes', (req, res) => {
    const header = req.header('access');
    const dados = req.body;

    if(header !== null && header !== undefined && header !== ''){
        if(header === '123456'){
            return res.json(dados);
        }else{
            return res.send('Token inválido!');
        }
    }else{
        return res.send('Token vazio!');
    }
});

// Rotas para funcionários
app.get('/funcionarios', (req, res) => {
    const source = req.query;

    return res.send(`Funcionário encontrado: ${source.nome}.`);
});

app.delete('/funcionarios/:nome', (req, res) => {
    const header = req.header('access');
    const nome = req.params.nome;

    if(header !== null && header !== undefined && header !== ''){
        if(header === '123456'){
            return res.send(`Usuário deletado: ${nome}.`);
        }else{
            return res.send('Token inválido!');
        }
    }else{
        return res.send('Token vazio!');
    }
});

app.put('/funcionarios', (req, res) => {
    const header = req.header('access');
    const dados = req.body;

    if(header !== null && header !== undefined && header !== ''){
        if(header === '123456'){
            return res.send(`Dados alterados. Novo nome: ${dados.nome}, novo sobrenome: ${dados.sobrenome}`);
        }else{
            return res.send('Token inválido!');
        }
    }else{
        return res.send('Token vazio!');
    }
})

app.listen(port, () => console.log('Servidor rodando...'));